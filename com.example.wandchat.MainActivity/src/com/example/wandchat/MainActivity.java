package com.example.wandchat;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {
	
	SharedPreferences mPrefs;
	static boolean a;
	

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        
        mPrefs = getSharedPreferences("prefs", 0);
      
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putBoolean("firstRun", true);
            editor.commit(); // Very important to save the preference
        if(a==false)
        {
        Thread t=new Thread()    	{
    		public void run()
    		{
    			try
    			{
    				int timer=0;
    				
    			while(timer<3000)
    			{
    				
    				sleep(100);
    				timer=timer+100;
    			}
    			
    			Intent intent=new Intent( MainActivity.this,first_wandchat.class);
    	    	startActivity(intent);
    	    
    			}
    		
    			
    			catch(InterruptedException e)
    			{
    				
    			}
    			finally
        		{
        			finish();
        		}
    		}
    		
    	};
    	
    	t.start();
    	a=true;
        
        }
        else {
        	finish();
        	Intent intent=new Intent( MainActivity.this,first_wandchat.class);
	    	startActivity(intent);
	    	
		}
       
    }


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		SharedPreferences     mPrefs=getSharedPreferences("prefs",0);
		boolean firstRun= mPrefs.getBoolean("firstRun", true);
		if(!firstRun){
			
			Intent intent=new Intent( MainActivity.this,first_wandchat.class);
	    	startActivity(intent);	
	}

	
	else{
		
		
		
	}
	} 
}
