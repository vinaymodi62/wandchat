package com.example.wandchat;

import java.util.concurrent.ThreadPoolExecutor.AbortPolicy;

import com.parse.Parse;
import com.parse.PushService;


public class Application extends android.app.Application {

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		
		
		Parse.initialize(this, Constants.APPLICATION_KEY, Constants.CLIENT_KEY); 

		// Specify an Activity to handle all pushes by default.
		PushService.setDefaultPushCallback(this,second_wandchat.class);
	}
	
	
	
	
	

}

