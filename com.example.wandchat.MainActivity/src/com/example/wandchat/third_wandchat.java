package com.example.wandchat;





import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;

public class third_wandchat extends Activity
{

	Button next,camera,gallery;
	ImageView imagecapture;
	private ProgressDialog pd;
	private Object data;
	private int resultcode;	
	static Bitmap bit;
	private static final int CAMERA_REQUEST = 11;
	private static final int YOUR_SELECT_PICTURE_REQUEST_CODE = 232;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.thirdscreen);
		
		next = (Button) findViewById(R.id.btnnext);
		imagecapture = (ImageView) findViewById(R.id.imagecapture);
		
		 
		
		imagecapture.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				
				
				// TODO Auto-generated method stub
				final Dialog dialog=new Dialog(third_wandchat.this);
			    dialog.setContentView(R.layout.dialogbox);
			   // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.show();
				camera = (Button) dialog.findViewById(R.id.camera);
				gallery = (Button) dialog.findViewById(R.id.gallery);
				
				
				
				camera.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dialog.cancel();
						Intent caintent =new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						startActivityForResult(caintent,CAMERA_REQUEST);
					}

					});
				
				gallery.setOnClickListener(new OnClickListener() {
					
					public void onClick(View v) {
						dialog.cancel();
						 Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
					        photoPickerIntent.setType("image/*");
					        startActivityForResult(photoPickerIntent, 1);
						
					}
				});
			}
			
		});
		
				
	
				
				
					 
	}		
	public void onActivityResult(int requestCode, int resultcode, Intent data)
	  {
		super.onActivityResult(requestCode, resultcode, data);
       if (requestCode == CAMERA_REQUEST) {
      	 Bitmap photo = (Bitmap) data.getExtras().get("data");
      	 photo = Bitmap.createScaledBitmap(photo, 150, 150, false);
           imagecapture.setImageBitmap(photo);
           bit=photo;
           
       }
       
       else if (resultcode == Activity.RESULT_CANCELED) {
           // User cancelled the image capture
       }
       
      else if  (requestCode == 1) 
       {
      	  if (data != null && resultcode == Activity.RESULT_OK)  
           {              
               
                 Uri selectedImage = data.getData();
                 
                 String[] filePathColumn = {MediaStore.Images.Media.DATA};
                 Cursor cursor = this.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                 cursor.moveToFirst();
                 int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                 String filePath = cursor.getString(columnIndex);
                 cursor.close();
                 if(bit != null && !bit.isRecycled())
                 {
                     bit = null;                
                 }
                 bit = BitmapFactory.decodeFile(filePath);
                 Bitmap bt = Bitmap.createScaledBitmap(bit, 150, 150, false);
                // imageView.setBackgroundResource(0);
                 imagecapture.setImageBitmap(bt);   
               }
      	  else 
           {
            Log.d("Status:", "Photopicker canceled");            
           }
       }
	
}
					 
				}
				
				
				
				
			
				
				
				
		
	
	
	
	


