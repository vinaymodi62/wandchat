package com.example.wandchat;

import org.json.JSONException;
import org.json.JSONObject;





import android.R.string;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class second_wandchat extends Activity 
{
static String json_String;
	EditText  edituser,editphone,editcode;
	Button btnentry,btnverify;
	String country,phone;
	Spinner sp_country;
	TextView codetext;
	String str_Country;
	String arr_code[];
	 
	private static final String url ="https://api.parse.com/1/classes/verification";
   
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second);
		

		sp_country =  (Spinner) findViewById(R.id.sp_Country);
		editphone = (EditText) findViewById(R.id.edit2);
		editcode = (EditText) findViewById(R.id.edit3);

		
		btnentry = (Button) findViewById(R.id.btntakeentry);
		btnverify = (Button) findViewById(R.id.btntakecode);
		codetext = (TextView) findViewById(R.id.countrycode);
		
		 ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
	     this, R.array.countries_array, android.R.layout.simple_spinner_item);
	     adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	     sp_country.setAdapter(adapter);
		
		 arr_code = getResources().getStringArray(R.array.countrycode);
		 
		 
		 
		 sp_country.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int possition, long arg3) {
				 str_Country = arg0.getItemAtPosition(possition).toString();
				 String s = arr_code[possition];
				 codetext.setText(s);
				
			}

			
			
			
			public void onNothingSelected(AdapterView<?> arg0) {
				
				
			}
		});
				
		btnentry.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
			
				country=null;
				phone=null;
				country = str_Country;
				phone = editphone.getText().toString();
				
				if(country.trim().length()==0 || phone.trim().length()==0)
				{	
					Toast.makeText(second_wandchat.this, "Username and Phone must be entered", Toast.LENGTH_SHORT).show();
					return;
				}
				
			else
			{
					try 
					{
						JSONObject jobj= new JSONObject();
					
			    	jobj.putOpt("uphoneno", phone);
			    	jobj.put("country",country);
						json_String=jobj.toString();
						
					Toast.makeText(getApplicationContext(), json_String, Toast.LENGTH_LONG).show();
					} 
					catch (JSONException e) 
					{
						
						e.printStackTrace();
					}
					
					if (Utils.isNetworkAvailable(second_wandchat.this)) {
						new Pars().execute(url);
						Toast.makeText(second_wandchat.this, "Record Updated", Toast.LENGTH_SHORT).show();

						editphone.setText("");
						
					} else {
						
						Toast.makeText(second_wandchat.this, "No Network Connection!!!", Toast.LENGTH_SHORT).show();
					}

				}
			}
		});
	}
	
private	class Pars extends AsyncTask<String, String, JSONObject>{

		@Override
		protected JSONObject doInBackground(String... params) {
			Parsepart2 p2 = new Parsepart2();
			String j1  = p2.getJsonstring(url);
			return null;
		}

		
		
	}
	

}

